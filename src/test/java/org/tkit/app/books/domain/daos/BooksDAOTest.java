package org.tkit.app.books.domain.daos;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.tkit.app.books.domain.models.criteria.BooksSearchCriteria;

import org.tkit.quarkus.jpa.exceptions.DAOException;
import static org.assertj.core.api.Assertions.*;

import javax.persistence.EntityManager;


@QuarkusTest
public class BooksDAOTest {
    @Test
    public void shouldThrowDAOExceptionWhenTryFindBookByCriteriaEqualNull() {
        //given
        BooksDAO booksDAO = new BooksDAO() {
            @Override
            protected EntityManager getEntityManager() {
                return null;
            }
        };
        BooksSearchCriteria searchCriteria = null;
        //when
        DAOException thrownException = Assertions.assertThrows(DAOException.class, () -> {
            booksDAO.searchByCriteria(searchCriteria);
        });
        //then
        assertThat(thrownException.key).isEqualTo(BooksDAO.ErrorKeys.ERROR_FIND_BOOKS_SEARCH_CRITERIA_REQUIRED);
    }

    @Test
    public void shouldThrowDAOExceptionWhenTryFindBookByEmptyCriteria() {
        //given
        BooksDAO booksDAO = new BooksDAO() {
            @Override
            protected EntityManager getEntityManager() {
                return null;
            }
        };
        BooksSearchCriteria searchCriteria = new BooksSearchCriteria();
        //when
        DAOException thrownException = Assertions.assertThrows(DAOException.class, () -> {
            booksDAO.searchByCriteria(searchCriteria);
        });
        //then
        assertThat(thrownException.key).isEqualTo(BooksDAO.ErrorKeys.ERROR_FIND_BOOKS_BY_CRITERIA);
    }

}
