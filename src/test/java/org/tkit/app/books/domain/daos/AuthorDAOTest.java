package org.tkit.app.books.domain.daos;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.tkit.app.books.domain.models.criteria.AuthorSearchCriteria;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import static org.assertj.core.api.Assertions.*;

import javax.persistence.EntityManager;

@QuarkusTest
public class AuthorDAOTest {
    @Test
    public void shouldThrowDAOExceptionWhenTryFindAuthorByCriteriaEqualNull() {
      
        AuthorDAO authorDAO = new AuthorDAO() {
            @Override
            protected EntityManager getEntityManager() {
                return null;
            }
        };
        AuthorSearchCriteria searchCriteria = null;
       
        DAOException thrownException = Assertions.assertThrows(DAOException.class, () -> {
            authorDAO.searchByCriteria(searchCriteria);
        });
        
        assertThat(thrownException.key).isEqualTo(AuthorDAO.ErrorKeys.ERROR_FIND_AUTHOR_SEARCH_CRITERIA_REQUIRED);
    }

    
   /* public void shouldThrowDAOExceptionWhenTryFindAuthorByEmptyCriteria() {
       
        AuthorDAO authorDAO = new AuthorDAO() {
            @Override
            protected EntityManager getEntityManager() {
                return null;
            }
        };
        AuthorSearchCriteria searchCriteria = new AuthorSearchCriteria();
        
        DAOException thrownException = Assertions.assertThrows(DAOException.class, () -> {
            authorDAO.searchByCriteria(searchCriteria);
        });
       
        assertThat(thrownException.key).isEqualTo(AuthorDAO.ErrorKeys.ERROR_FIND_AUTHOR_BY_CRITERIA);
    } */

}
