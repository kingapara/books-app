package org.tkit.app.books.rs.v1controllers;


import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.tkit.app.books.rs.v1.models.AuthorCreateUpdateDTO;
import org.tkit.app.books.rs.v1.models.AuthorDTO;
import org.tkit.app.books.rs.v1.models.criteria.BooksSearchCriteriaDTO;
import org.tkit.app.test.AbstractTest;
import org.tkit.quarkus.rs.models.PageResultDTO;
import org.tkit.quarkus.test.WithDBData;

import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@WithDBData(value= {"books-app-test-data.xls"}, deleteBeforeInsert = true, rinseAndRepeat = true)
public class AuthorRestControllerTest extends AbstractTest {
    private static final String EXISTING_AUTHOR_NAME="Jan";
    private static final String EXISTING_AUTHOR_SURNAME="Kowalski";
    private static final int AUTHOR_AGE= 36;
    private static final int AMOUNT_OF_AUTHOR = 5;
    private static final String ID = "author5";

   
    @Test
    @DisplayName("Get author  by id")
    public void testSuccessfullGetAuthor() {
        Response response = given()
                .contentType(APPLICATION_JSON)
                .when()
                .get("/author/" + ID);
        response.then()
                .statusCode(200);
       AuthorDTO authorDTO = response.as(AuthorDTO.class);
        assertThat(authorDTO.getAuthorName()).isEqualTo(EXISTING_AUTHOR_NAME);
        assertThat(authorDTO.getAuthorSurname()).isEqualTo(EXISTING_AUTHOR_SURNAME);
        assertThat(authorDTO.getAge()).isEqualTo(AUTHOR_AGE);
    }
    
   
    @Test
    @DisplayName("Delete author")
    public void testSuccessfullDeleteAuthor() {
        Response deleteResponse = given().
                when().delete("/author/" + ID);
        deleteResponse.then().
                statusCode(NO_CONTENT.getStatusCode());
        Response getResponse = given()
                .when()
                .get("/author");
        getResponse.then().statusCode(200);
        PageResultDTO<AuthorDTO> pageResultDTO = getResponse.as(getAuthorDTOTypeRef());
        assertThat(pageResultDTO.getStream().size()).isEqualTo(AMOUNT_OF_AUTHOR - 1);
    }

    private TypeRef<PageResultDTO<AuthorDTO>> getAuthorDTOTypeRef() {
        return new TypeRef<>() {
        };
    }



}
