package org.tkit.app.books.rs.v1controllers;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.tkit.app.books.domain.models.enums.CategoryType;
import org.tkit.app.books.rs.v1.models.BooksDTO;
import org.tkit.app.test.AbstractTest;
import org.tkit.app.books.rs.rfc.RFCProblemDTO;
import org.tkit.quarkus.rs.models.PageResultDTO;



import org.tkit.quarkus.test.WithDBData;
import static org.assertj.core.api.Assertions.*;
import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;


@QuarkusTest
@WithDBData(value = {"books-app-test-data.xls"}, deleteBeforeInsert = true, rinseAndRepeat = true)
public class BooksRestControllerGetDeleteTest extends AbstractTest{
  private static final String BOOK_TITLE = "prokurator";
  private static final Long BOOK_ISBN = 9788328706309l;
  private static final int BOOK_TOTAL_PAGE = 320;
  private static final int DEFAULT_PAGE_SIZE = 100;
  private static final int BOOK_COUNT = 4;
  private static final int TOTAL_PAGES = 1;
  private static final Long EXISTING_BOOK_ID = 40l;
  private static final Long NOT_EXISTING_BOOK_ID = 123l;


@Test
@DisplayName("Get book by Id")
public void testSuccessfullGetBook() {
    Response response = given()
    .contentType(APPLICATION_JSON)
    .when()
    .get("/book/" + EXISTING_BOOK_ID );
response.then()
    .statusCode(200);
BooksDTO book = response.body().as(BooksDTO.class);
assertThat(book.getTitle()).isEqualTo(BOOK_TITLE);
assertThat(book.getIsbn()).isEqualTo(BOOK_ISBN);
assertThat(book.getCategoryType()).isEqualTo(CategoryType.DRAMA);
assertThat(book.getTotalPage()).isEqualTo(BOOK_TOTAL_PAGE);
assertThat(book.getAuthorDTO());
}


@Test
@DisplayName("Not find book by ID")
public void testFailedGetBook() {
    Response response = given()
            .contentType(APPLICATION_JSON)
            .when()
            .get("/book/" + NOT_EXISTING_BOOK_ID);
    response.then()
            .statusCode(404);
    RFCProblemDTO rfcProblemDTO = response.as(RFCProblemDTO.class);
    assertThat(rfcProblemDTO.getStatus().toString()).isEqualTo("404");
    assertThat(rfcProblemDTO.getDetail()).isEqualTo("Book not found.");
    assertThat(rfcProblemDTO.getTitle()).isEqualTo("TECHNICAL ERROR");
    assertThat(rfcProblemDTO.getType()).isEqualTo("REST_EXCEPTION");
}

    @Test
    @DisplayName("Find book by incorrect ID")
    public void testFailedGetBookByIncorrectId() {
        Response response = given()
                .when()
                .get("/book/-1");
        response.then()
                .statusCode(404);
        RFCProblemDTO rfcProblemDTO = response.as(RFCProblemDTO.class);
        assertThat(rfcProblemDTO.getStatus().toString()).isEqualTo("404");
        assertThat(rfcProblemDTO.getDetail()).isEqualTo("Book not found.");
        assertThat(rfcProblemDTO.getTitle()).isEqualTo("TECHNICAL ERROR");
        assertThat(rfcProblemDTO.getType()).isEqualTo("REST_EXCEPTION");
    }

    @Test
    @DisplayName("Find by not existing criteria parameter")
    public void testSuccessfulFindBooksByNoExistingCriteria() {
        Response response = given()
                .when()
               // .queryParam("noExistingParam", "value")
               .get("book" + "/?" + "noExistingParam" + "=" + "value");
               // .get("/book");
        response.then().statusCode(200);
        PageResultDTO pageResultDTO = response.as(PageResultDTO.class);
        assertThat(pageResultDTO.getSize()).isEqualTo(DEFAULT_PAGE_SIZE);
        assertThat(pageResultDTO.getTotalElements()).isEqualTo(BOOK_COUNT);
        assertThat(pageResultDTO.getTotalPages()).isEqualTo(TOTAL_PAGES);
        assertThat(pageResultDTO.getStream().size()).isEqualTo(BOOK_COUNT);
    }


    @Test
    @DisplayName("Gets all books")
    public void testSuccessfulFindBooksByNoCriteria() {
        Response response = given()
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .get("book/");
        response.then().statusCode(200);
        PageResultDTO pageResultDTO = response.as(PageResultDTO.class);
        assertThat(pageResultDTO.getSize()).isEqualTo(DEFAULT_PAGE_SIZE);
        assertThat(pageResultDTO.getTotalElements()).isEqualTo(BOOK_COUNT);
        assertThat(pageResultDTO.getTotalPages()).isEqualTo(TOTAL_PAGES);
        assertThat(pageResultDTO.getStream().size()).isEqualTo(BOOK_COUNT);
    }



}
