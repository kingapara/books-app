package org.tkit.app.books.domain.models.criteria;

import lombok.Getter;
import lombok.Setter;
import org.tkit.app.books.domain.models.enums.CategoryType;




@Getter
@Setter
public class BooksSearchCriteria {
    
    private String title;

    private CategoryType categoryType;

    private Long isbn;

    private Integer  totalPage;

    private String authorName;

    private String authorSurname;

    private Integer pageNumber;

    private Integer pageSize;
    
}
