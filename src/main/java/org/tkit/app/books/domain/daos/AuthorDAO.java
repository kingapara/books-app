package org.tkit.app.books.domain.daos;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


import org.tkit.app.books.domain.models.Author;
import org.tkit.app.books.domain.models.Author_;
import org.tkit.app.books.domain.models.criteria.AuthorSearchCriteria;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.exceptions.DAOException;

@ApplicationScoped
public class AuthorDAO extends AbstractDAO<Author> {

    enum ErrorKeys {
        ERROR_FIND_AUTHOR_BY_CRITERIA,
        ERROR_FIND_AUTHOR_SEARCH_CRITERIA_REQUIRED
    }

    /**
     * Finds a {@link Author} matching the given {@link AuthorSearchCriteria}.
     *
     * @param criteria the {@link AuthorSearchCriteria}
     * @return the  {@link Author}
     */
    public PageResult<Author> searchByCriteria(AuthorSearchCriteria criteria) {
        if (criteria == null) {
            throw new DAOException(ErrorKeys.ERROR_FIND_AUTHOR_SEARCH_CRITERIA_REQUIRED, new NullPointerException());
        }
        try {
            CriteriaQuery<Author> cq = criteriaQuery();
            Root<Author> root = cq.from(Author.class);
            List<Predicate> predicates = new ArrayList<>();
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            if (criteria.getAuthorName() != null && !criteria.getAuthorName().isEmpty()) {
                predicates.add(cb.like(cb.lower(root.get(Author_.AUTHOR_NAME)),stringPattern(criteria.getAuthorName())));
            }
            if (criteria.getAuthorSurname() != null  && !criteria.getAuthorSurname().isEmpty()) {
                predicates.add(cb.like(cb.lower(root.get(Author_.AUTHOR_SURNAME)),stringPattern(criteria.getAuthorSurname())));
            }
            if (!predicates.isEmpty()) {
                cq.where(predicates.toArray(new Predicate[0]));
            }

            return createPageQuery(cq, Page.of(criteria.getPageNumber(), criteria.getPageSize())).getPageResult();
        } catch (Exception e) {
            throw new DAOException(ErrorKeys.ERROR_FIND_AUTHOR_BY_CRITERIA, e);
        }
    }
    private String stringPattern(String value) {
    return (value.toLowerCase() + "%");
}
}

