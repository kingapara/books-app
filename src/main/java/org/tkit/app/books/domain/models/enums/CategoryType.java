package org.tkit.app.books.domain.models.enums;

public enum CategoryType {
    HORROR, 
    DRAMA, 
    ROMANCE, 
    FANTASY,
    LITERATURE,
    THRILLER,
    REPORT,
}