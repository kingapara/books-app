package org.tkit.app.books.domain.models;
import org.tkit.quarkus.jpa.models.TraceableEntity;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "AUTHOR")
public class Author extends TraceableEntity {
    
 @Column(name = "AUTHOR_NAME")
 private String authorName;

 @Column( name = "AUTHOR_SURNAME", unique = true)
 private String authorSurname;

 @Column( name = "AGE")
 private Integer age;

}
