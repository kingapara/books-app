package org.tkit.app.books.domain.models;


import javax.persistence.Table;
import javax.persistence.*;

import org.tkit.app.books.domain.models.enums.CategoryType;
import org.tkit.quarkus.jpa.models.BusinessTraceableEntity;


import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "BOOK")
public class Book extends BusinessTraceableEntity {

    @Column(name = "TITLE", nullable= false)
    private String title;

    @Column(name = "TOTAL_PAGE")
    private Integer totalPage;

    @Column( name = "ISBN", unique = true)
    private Long isbn;

    @Column(name = "CATEGORY")
    private CategoryType categoryType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTHOR")
    private org.tkit.app.books.domain.models.Author author;

}
