package org.tkit.app.books.domain.models.criteria;



import lombok.Getter;
import lombok.Setter;



@Getter
@Setter

public class AuthorSearchCriteria {
    
   private String authorName;

   private String authorSurname;

   private Integer PageNumber;

   private Integer PageSize;
}
