package org.tkit.app.books.domain.daos;

import org.tkit.app.books.domain.models.*;
import org.tkit.app.books.domain.models.criteria.BooksSearchCriteria;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class BooksDAO extends AbstractDAO<Book> {
   
    public enum ErrorKeys {
        ERROR_FIND_BOOKS_BY_CRITERIA,
        ERROR_FIND_BOOKS_SEARCH_CRITERIA_REQUIRED;
    }

    public PageResult<Book> searchByCriteria(BooksSearchCriteria criteria) {
        if (criteria == null) {
            throw new DAOException(ErrorKeys.ERROR_FIND_BOOKS_SEARCH_CRITERIA_REQUIRED, new NullPointerException());
        }
        try {
            CriteriaQuery<Book> cq = criteriaQuery();
            Root<Book> root = cq.from(Book.class);
            List<Predicate> predicates = new ArrayList<>();
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            if (criteria.getTitle() != null && !criteria.getTitle().isEmpty()) {
                predicates.add(cb.like(cb.lower(root.get(Book_.TITLE)),stringPattern(criteria.getAuthorName())));
            }
            if (criteria.getIsbn() != null) {
                predicates.add(cb.like(cb.lower(root.get(Book_.ISBN)),stringPattern(criteria.getAuthorName())));
            }
        
            if (criteria.getTotalPage() != null) {
                predicates.add(cb.like(cb.lower(root.get(Book_.TOTAL_PAGE)),stringPattern(criteria.getAuthorName())));
            }
            if (criteria.getCategoryType() != null) {
                predicates.add(cb.like(cb.lower(root.get(Book_.CATEGORY_TYPE)),stringPattern(criteria.getAuthorName())));
            }
            if (criteria.getAuthorName() != null && !criteria.getAuthorName().isEmpty()) {
                predicates.add(cb.like(cb.lower(root.get(Book_.AUTHOR)),stringPattern(criteria.getAuthorName())));
            }
            if (criteria.getAuthorSurname() != null && !criteria.getAuthorSurname().isEmpty()) {
                predicates.add(cb.like(cb.lower(root.get(Book_.AUTHOR)),stringPattern(criteria.getAuthorName())));
            }

            if (!predicates.isEmpty()) {
                cq.where(predicates.toArray(new Predicate[0]));
            }
            return createPageQuery(cq, Page.of(criteria.getPageNumber(), criteria.getPageSize())).getPageResult();
        } catch (Exception exception) {
            throw new DAOException(ErrorKeys.ERROR_FIND_BOOKS_BY_CRITERIA, exception);
        }
    }
    private String stringPattern(String value) {
        return (value.toLowerCase() + "%");
    }
}
