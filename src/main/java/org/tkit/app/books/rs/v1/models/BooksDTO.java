package org.tkit.app.books.rs.v1.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.tkit.app.books.domain.models.enums.CategoryType;
import org.tkit.quarkus.rs.models.BusinessTraceableDTO;
import javax.validation.constraints.NotBlank;


@Getter
@Setter
@ToString
public class BooksDTO extends BusinessTraceableDTO {
    @NotBlank(message = "must not be blank")
    @Schema(description = "The book's name.")
    private String title;
    

    @Schema(description = "The book's isbn.")
    private Long isbn;
  
    @Schema(description = "The book's category.")
    private CategoryType categoryType;

    @Schema(description = "The book's total page.")
    private Integer totalPage;

    @NotBlank(message = "must not be blank")
    @Schema(description = "The book's author.")
   private AuthorDTO authorDTO;

    
}

