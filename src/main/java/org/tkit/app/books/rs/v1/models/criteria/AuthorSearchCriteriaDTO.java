package org.tkit.app.books.rs.v1.models.criteria;
import lombok.Getter;
import lombok.Setter;

import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
public class AuthorSearchCriteriaDTO {
    @QueryParam("authorName")
    private String authorName;

    @QueryParam("authorSurname")
    private String authorSurname;
    
    @DefaultValue("0")
    @QueryParam("pageNumber")
    @Schema(description = "The number of page.")
    private  Integer pageNumber = 0;

    @DefaultValue("100")
    @QueryParam("pageSize")
    @Schema(description = "The size of page")
    private Integer pageSize = 100;

}
