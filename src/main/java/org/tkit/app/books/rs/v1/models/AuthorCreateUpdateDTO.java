package org.tkit.app.books.rs.v1.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
public class AuthorCreateUpdateDTO {
    @NotBlank(message = "must not be blank")
    @Schema(description = "The Name")
    private String authorName;

    @NotBlank(message = "must not be blank")
    @Schema(description = "The Surname.")
    private String authorSurname;

    @NotBlank(message = "must not be blank")
    @Schema(description = "The Age.")
    private Integer age;
}
