package org.tkit.app.books.rs.v1.models.criteria;

import lombok.Getter;
import lombok.Setter;
import org.tkit.app.books.domain.models.enums.CategoryType;
import javax.ws.rs.DefaultValue;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.ws.rs.QueryParam;


@Getter
@Setter
public class BooksSearchCriteriaDTO {
    
    @QueryParam("title")
    private String title;

    @QueryParam("isbn")
    private Long isbn;

    @QueryParam("category")
    private CategoryType categoryType;

    @QueryParam("totalPage")
    private Integer totalPage;

    @QueryParam("authorName")
    private String authorName;

    @QueryParam("authorSurname")
    private String authorSurname;

    @DefaultValue("0")
    @QueryParam("pageNumber")
    @Schema(description = "The number of page.")
    private  Integer pageNumber = 0;

    @DefaultValue("100")
    @QueryParam("pageSize")
    @Schema(description = "The size of page")
    private Integer pageSize = 100;

}
