package org.tkit.app.books.rs.v1.controllers;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.tkit.app.books.domain.daos.BooksDAO;
import org.tkit.app.books.domain.daos.AuthorDAO;
import org.tkit.app.books.domain.models.Book;
import org.tkit.app.books.domain.models.Author;
import org.tkit.app.books.domain.models.criteria.BooksSearchCriteria;
import org.tkit.app.books.rs.v1.mappers.AuthorMapper;
import org.tkit.app.books.rs.v1.mappers.BooksMapper;
import org.tkit.app.books.rs.v1.models.BooksCreateUpdateDTO;
import org.tkit.app.books.rs.v1.models.BooksDTO;
import org.tkit.app.books.rs.v1.models.criteria.BooksSearchCriteriaDTO;
import org.tkit.app.books.rs.rfc.RFCProblemDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.exceptions.RestException;
import org.tkit.quarkus.rs.mappers.OffsetDateTimeMapper;
import org.tkit.quarkus.rs.models.PageResultDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("/book")
@ApplicationScoped
@Tag(name = "Books REST")
@Produces(MediaType.APPLICATION_JSON)


public class BooksRestController {
    @Inject
    BooksDAO booksDAO;

    @Inject
    AuthorDAO authorDAO;

    @Inject
    BooksMapper booksMapper;

    @Inject
    AuthorMapper authorMapper;

    @Inject
    OffsetDateTimeMapper offsetDateTimeMapper;

    @GET
    @Path("/{id}")
    @Operation(operationId = "getBookById", description = "Get book by Id")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Ok",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BooksDTO.class))),
            @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    })
    public Response getBook(@PathParam("id") Long id) {
        Book book = booksDAO.findById(id);
        if (book == null) {
            throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Book not found.");
        }
        return Response.status(Response.Status.OK)
                       .type(MediaType.APPLICATION_JSON_TYPE)
                        .entity(booksMapper.mapToDTO(book))
                        .build();
    }

    @GET
    @Operation(operationId = "getBookByCriteria", description = "Gets book by criteria")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The corresponding books resources",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = PageResultDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
    })
    public Response getBooksByCriteria(@BeanParam BooksSearchCriteriaDTO criteriaDTO) {
        BooksSearchCriteria criteria = booksMapper.mapToSearchCriteria(criteriaDTO);
        PageResult<Book> book = booksDAO.searchByCriteria(criteria);

        return Response.ok(booksMapper.mapToPageResultDTO(book)).build();

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Operation(operationId = "createBook", description = "Create the book")
    @APIResponses({
            @APIResponse(responseCode = "201", description = "Created book resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BooksDTO.class)),
                    headers = {@Header(name = "Location", description = "URL for the create book resource")}),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
    })
    public Response saveBook(@Valid BooksCreateUpdateDTO booksDTO) {
           Author author = authorDAO.findById(booksDTO.getAuthorSurname());
            if (Objects.nonNull(author)) {
                Book bookEntity = booksMapper.mapToEntity(booksDTO);
                    return Response.status(Response.Status.CREATED)
                            .entity(booksMapper.mapToDTO(booksDAO.create(bookEntity)))
                            .build();
                }
                throw new RestException(Response.Status.BAD_REQUEST, Response.Status.BAD_REQUEST, "books is not available.");
            }
           
        

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @Transactional
    @Operation(operationId = "updateBook", description = "Update the book")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Updated book resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BooksDTO.class))),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "404", description = "Not Found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
    })
    public Response updateBook(@PathParam("id") Long id, @Valid BooksCreateUpdateDTO booksDTO) {
      
        Book book = booksDAO.findById(id);
        if (Objects.nonNull(book)) {
           Author author = authorDAO.findById(booksDTO.getTitle());
            if (Objects.nonNull(author)){
                booksMapper.updateBookFromDTO(booksDTO, book);
                return Response.status(Response.Status.CREATED)
                               .entity(booksMapper.mapToDTO(booksDAO.update(book)))
                               .build();
            }
            throw new RestException(Response.Status.BAD_REQUEST, Response.Status.BAD_REQUEST, "Book is not available.");
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Book not found.");
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Operation(operationId = "deleteBook", description = "Delete book")
    @APIResponses({
            @APIResponse(responseCode = "204", description = "Deleted book resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON)),
            @APIResponse(responseCode = "404", description = "Not Found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    })
    public Response deleteBook(@PathParam("id") Long id) {
        Book book = booksDAO.findById(id);
        if (Objects.nonNull(book)) {
            booksDAO.delete(book);
            return Response.status(Response.Status.NO_CONTENT)
                           .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Book not found.");
    }

   

   
}


