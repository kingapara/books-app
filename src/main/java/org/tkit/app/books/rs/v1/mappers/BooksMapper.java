package org.tkit.app.books.rs.v1.mappers;

import org.mapstruct.*;
import org.tkit.app.books.domain.models.Book;
import org.tkit.app.books.domain.models.criteria.BooksSearchCriteria;
import org.tkit.app.books.rs.v1.models.BooksCreateUpdateDTO;
import org.tkit.app.books.rs.v1.models.BooksDTO;
import org.tkit.app.books.rs.v1.models.criteria.BooksSearchCriteriaDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.mappers.OffsetDateTimeMapper;
import org.tkit.quarkus.rs.models.PageResultDTO;

@Mapper(componentModel = "cdi", uses = { AuthorMapper.class, 
        OffsetDateTimeMapper.class}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface BooksMapper {
        BooksDTO mapToDTO(Book book);
        Book mapToEntity(BooksCreateUpdateDTO book);
    
        BooksSearchCriteria mapToSearchCriteria(BooksSearchCriteriaDTO searchCriteria);
    
        PageResultDTO<BooksDTO> mapToPageResultDTO(PageResult<Book> page);
    
        @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
                nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
        @Mapping(target = "id", ignore = true)
       
        Book updateBookFromDTO(BooksCreateUpdateDTO booksDTO, @MappingTarget Book book);
    }
    
    
