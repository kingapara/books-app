package org.tkit.app.books.rs.v1.mappers;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import org.mapstruct.MappingTarget;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.mappers.OffsetDateTimeMapper;
import org.tkit.quarkus.rs.models.PageResultDTO;
import org.tkit.app.books.domain.models.Author;
import org.tkit.app.books.domain.models.criteria.AuthorSearchCriteria;
import org.tkit.app.books.rs.v1.models.AuthorCreateUpdateDTO;
import org.tkit.app.books.rs.v1.models.AuthorDTO;
import org.tkit.app.books.rs.v1.models.criteria.AuthorSearchCriteriaDTO;

import java.util.List;

@Mapper(componentModel = "cdi", uses= OffsetDateTimeMapper.class, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface AuthorMapper {
    
    AuthorDTO mapToDTO(Author author);

    List<AuthorDTO> mapToListDTO(List<Author> author);

    Author mapToEntity(AuthorCreateUpdateDTO author);

    List<Author> mapToListEntity(List<AuthorCreateUpdateDTO> author);

    AuthorSearchCriteria mapToSearchCriteria(AuthorSearchCriteriaDTO searchCriteria);

    PageResultDTO<AuthorDTO> mapToPageResultDTO(PageResult<Author> page);

    Author updateAuthorFromDTO(AuthorCreateUpdateDTO AuthorDTO, @MappingTarget Author author);
}


