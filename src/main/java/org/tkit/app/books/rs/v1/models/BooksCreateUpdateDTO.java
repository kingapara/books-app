package org.tkit.app.books.rs.v1.models;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.tkit.app.books.domain.models.enums.CategoryType;
import org.tkit.app.books.domain.models.Author;
import javax.validation.constraints.*;



@Getter
@Setter
public class BooksCreateUpdateDTO {
    @NotBlank(message = "must not be blank")
    @Schema(description = "The book's name.")
    private String title;
   
    @Schema(description = "The book's isbn.")
    private Long isbn;

    @Schema(description = "The book's category.")
    private CategoryType categoryType;

    @Schema(description = "The book's total page.")
    private Integer totalPage;
    
    @NotBlank(message = "must not be blank")
    @Schema(description = "The name of the author of the book")
    private Author authorName;

    @NotNull(message = "must not be null")
    @Schema(description = "The Surname of the author of the book")
    private Author authorSurname;

    @Schema(description = "The age of the author of the book")
    private Author authorAge;


}
