package org.tkit.app.books.rs.v1.controllers;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.tkit.app.books.domain.daos.BooksDAO;
import org.tkit.app.books.domain.daos.AuthorDAO;
import org.tkit.app.books.domain.models.Book;
import org.tkit.app.books.domain.models.Author;
import org.tkit.app.books.domain.models.criteria.AuthorSearchCriteria;
import org.tkit.app.books.rs.v1.mappers.AuthorMapper;
import org.tkit.app.books.rs.v1.models.AuthorCreateUpdateDTO;
import org.tkit.app.books.rs.v1.models.AuthorDTO;
import org.tkit.app.books.rs.v1.models.criteria.AuthorSearchCriteriaDTO;
import org.tkit.app.books.rs.rfc.RFCProblemDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.exceptions.RestException;
import org.tkit.quarkus.rs.models.PageResultDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Path("/author")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "author REST")


public class AuthorRestController {
    @Inject
    AuthorDAO authorDAO;

    @Inject
    BooksDAO booksDAO;

    @Inject
    AuthorMapper authorMapper;

    @GET
    @Path("/{id}")
    @Operation(operationId = "getAuthorById", description = "Get Author by ID")
    @APIResponses({
            @APIResponse(responseCode = "200",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class))),
            @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    })
    public Response getAuthorById(@PathParam("id") String id) {
        Author author = authorDAO.findById(id);
        if (author == null) {
            throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "The Author with given id not found.");
        }
        return Response.status(Response.Status.OK)
                       .type(MediaType.APPLICATION_JSON_TYPE)
                        .entity(authorMapper.mapToDTO(author))
                        .build();
    }

    @GET
    @Operation(operationId = "getAuthorByCriteria", description = "Get Author by criteria")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The Author's resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = PageResultDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    })
    public Response getAuthorByCriteria(@BeanParam AuthorSearchCriteriaDTO searchCriteriaDTO) {
       AuthorSearchCriteria searchCriteria = authorMapper.mapToSearchCriteria(searchCriteriaDTO);
        PageResult<Author> pageAuthor = authorDAO.searchByCriteria(searchCriteria);
        return Response.ok(authorMapper.mapToPageResultDTO(pageAuthor)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Operation(operationId = "createAuthor", description = "Create Author")
    @APIResponses({
            @APIResponse(responseCode = "201", description = "Created Author resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class)),
                    headers = {@Header(name = "Location", description = "URL for the create Author resource")}),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    })
    public Response saveAuthor(@Valid AuthorCreateUpdateDTO AuthorDTO) {
        Book book = booksDAO.findById(AuthorDTO.getAuthorSurname());
        if(Objects.nonNull(book)){
            Author authorEntity = authorMapper.mapToEntity(AuthorDTO);
            return Response.status(Response.Status.CREATED)
                    .entity(authorMapper.mapToDTO(authorDAO.create(authorEntity)))
                    .build();
        }
        throw new RestException(Response.Status.BAD_REQUEST, Response.Status.BAD_REQUEST, " The Book for given id does not exist.");
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Operation(operationId = "updateAuthor", description = "Update Author")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Updated Author resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class))),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "404", description = "Not Found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    })
    public Response updateAuthor(@PathParam("id") String id, @Valid AuthorCreateUpdateDTO authorDTO) {
        Author author = authorDAO.findById(id);
        if (Objects.nonNull(author)) {
            Book book = booksDAO.findById(authorDTO.getAuthorSurname());
            if (Objects.nonNull(book)) {
                authorMapper.updateAuthorFromDTO(authorDTO, author);
                return Response.status(Response.Status.CREATED)
                        .entity(authorMapper.mapToDTO(authorDAO.update(author)))
                        .build();
            }
            throw new RestException(Response.Status.BAD_REQUEST, Response.Status.BAD_REQUEST, " The Book for given id does not exist.");
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "The Author with given id not found.");
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Operation(operationId = "deleteAuthor", description = "Delete Author")
    @APIResponses({
            @APIResponse(responseCode = "204", description = "Deleted Author resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class))),
            @APIResponse(responseCode = "404", description = "Not Found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    })
    public Response deleteAuthor(@PathParam("id") String id) {
        Author author = authorDAO.findById(id);
        if (Objects.nonNull(author)) {
            authorDAO.delete(author);
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "The Author with given id not found.");
    }

}
