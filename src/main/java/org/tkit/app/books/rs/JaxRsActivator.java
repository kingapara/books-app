package org.tkit.app.books.rs;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.servers.Server;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@OpenAPIDefinition(
        servers = {
                @Server(url = "http://localhost:8080/")
        },
        info = @Info(title = "books-app", description = "simple book application", version = "1.0")
)
@ApplicationPath("/")
public class JaxRsActivator extends Application {
}
